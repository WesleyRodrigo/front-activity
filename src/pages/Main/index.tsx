import {Container} from "./styles";
import Menu from "../../components/Menu";
import ComponentFrame from "../../components/ComponentFrame";
import {useEffect, useState} from "react";

const Main = () => {
    const [reset, setReset] = useState(false)
    const [spaceFrame1, setSpaceFrame1] = useState(40)
    const [spaceFrame2, setSpaceFrame2] = useState(40)

    useEffect(() => {
        listenner()
    }, [])

    const trigger = (event: any)  => {
        if(event.data.message === 'frame1'){
            setSpaceFrame1(spaceFrame1 + event.data.space)
            setSpaceFrame2(spaceFrame2 - event.data.space)
        }

        if (event.data.message === 'frame2') {
            setSpaceFrame1(spaceFrame1 - event.data.space)
            setSpaceFrame2(spaceFrame2 + event.data.space)
        }

        if (event.data.message === 'imagem'){
            window.frames[1].postMessage({message: 'imagem', url: event.data.img}, '*' )
        }
    }

    const listenner = () => {
        window.addEventListener('message', trigger)
    }

    return <Container>
        <Menu handleReset={setReset} setFrame1={setSpaceFrame1} setFrame2={setSpaceFrame2}/>
        <div className="frame-container">
            <ComponentFrame space={spaceFrame1} handleReset={setReset} reset={reset}
                            source='http://localhost:3000/interface_one' title="Interface 1"/>
            <ComponentFrame space={spaceFrame2} handleReset={setReset} reset={reset}
                            source='http://localhost:3000/interface_two' title="Interface 2"/>
        </div>
    </Container>
};

export default Main