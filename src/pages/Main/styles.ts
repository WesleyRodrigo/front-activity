import styled from "styled-components";

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  background-image: linear-gradient(to right top, #047a28, #5b7e16, #8a7f1b, #b17f33, #cf7f54, #df875d, #ef8e66, #ff966f, #ffaa58, #ffc33b, #fde018, #deff00);
  
  div.frame-container {
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: space-around;
  }
`