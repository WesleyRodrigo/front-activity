import styled from "styled-components";

export const InterfaceOneContainer = styled.div`
  width: 100vw;
  height: 100vh;

  background: ${(props: { color: string; }) => props.color ? props.color : '#dedede'};
  box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
  backdrop-filter: blur(6.0px);
  -webkit-backdrop-filter: blur(6.0px);
  border-radius: 10px;
  
  button{
    :hover{
      cursor: pointer;
    }
  }
`