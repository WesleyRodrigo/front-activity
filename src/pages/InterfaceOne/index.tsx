import {useEffect, useState} from "react";
import {InterfaceOneContainer} from "./styles";

const InterfaceOne = ({reset}: any) => {
    const [color, setColor] = useState('')
    const [url, setUrl] = useState('')

    useEffect(() => {
        listen()
    }, [])

    const trigger = (event: any) => {
        setColor(event.data.color)
    }

    const listen = () => {
        window.addEventListener('message', trigger)
    }

    const handleSpace = () => {
        window.parent.postMessage({message:'frame1', space:20}, '*')
    }

    const sendImg = () => {
        window.parent.postMessage({message: 'imagem', img: url}, '*')
    }

    return <InterfaceOneContainer color={color}>
        <button onClick={handleSpace}>Mais Espaço</button>
        <div>
            <input placeholder='URL da imagem' onChange={(event: any) => setUrl(event.target.value)}/>
            <button onClick={sendImg}>Enviar</button>
        </div>
    </InterfaceOneContainer>
}

export default InterfaceOne
