import styled from "styled-components";

interface IProps {
    color: string
    url: string
}

// @ts-ignore
export const InterfaceTwoContainer = styled.div`
  background-color: ${(props: { color: string; }) => props.color ? props.color : '#dedede'};
  background-image: ${(props: { url: string }) => props.url && `url(${props.url})`};
  background-position: center;
  background-repeat: no-repeat;

  box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
  backdrop-filter: blur(6.0px);
  -webkit-backdrop-filter: blur(6.0px);
  border-radius: 10px;
  width: 100vw;
  height: 100vh;

  button {
    :hover {
      cursor: pointer;
    }
  }
`