import {InterfaceTwoContainer} from "./styles";
import {useEffect, useState} from "react";

const InterfaceTwo = () => {
    const [ color, setColor] = useState('')
    const [url, setUrl] = useState('')

    useEffect(() => {
        listen()
    }, [])

    const trigger = (event: any) => {
        if(event.data.message === `change color`){
            setColor(event.data.color)
        }

        if(event.data.message === 'reset'){
            setColor(event.data.color)
        }

        if (event.data.message === 'imagem'){
            setUrl(event.data.url)
        }

    }

    const listen = () => {
        window.addEventListener('message', trigger)
    }

    const handleSpace = () => {
        window.parent.postMessage({message:'frame2', space:20}, '*')
    }

    // @ts-ignore
    return <InterfaceTwoContainer color={color} url={url}>
        <button onClick={handleSpace}>Mais Espaço</button>
    </InterfaceTwoContainer>
}

export default InterfaceTwo