import styled from "styled-components";
interface IProps {
    space?: number
}
export const Container = styled.div`
  width: ${ (props: IProps) => props.space && props.space + '%'}; 
  height: 80%;
  
  iframe{
    width: 100%;
    height: 100%;
  }
`