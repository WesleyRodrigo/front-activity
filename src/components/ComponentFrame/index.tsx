import { useEffect, useState } from "react";
import {Container} from "./styles";

interface IProps{
    source: string
    title: string
    space?: number
    reset: boolean
    handleReset: any
}

const ComponentFrame = ( {source , title, space, reset, handleReset}: IProps ) => {
    const [width, setWidth] = useState(space)

    useEffect(() => {
        console.log(space,reset)
        if(reset){
            setWidth(40)
            handleReset(false)
        }
    }, [reset, space, width])

    return <Container space={space}>
        <iframe src={source} title={title}/>
    </Container>
}

export default ComponentFrame