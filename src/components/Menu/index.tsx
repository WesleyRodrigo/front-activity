import {MenuContainer} from "./styles";

const Menu = ({handleReset, setFrame1, setFrame2}: any) => {

    const randomColor = () => {
        const letters = "0123456789ABCDEF";
        let color = "#";
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    const handleClickOne = () => {
        let frame = Math.floor(Math.random() * 2)
        let color = randomColor()
        window.frames[frame].postMessage({message:'change color', color:color}, '*')
    }

    const handleClickTwo = () => {
        for(let i = 0; i < window.frames.length; i++){
            window.frames[i].postMessage({message:'reset', color:''}, '*')
        }
        handleReset(true)
        setFrame1(40)
        setFrame2(40)
    }

    return <MenuContainer>
        <div>
            <button onClick={handleClickOne} className='Button'>Color</button>
            <button onClick={handleClickTwo} className='Button'>Reset</button>
        </div>
    </MenuContainer>
}

export default Menu