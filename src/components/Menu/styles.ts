import styled from "styled-components";

export const MenuContainer = styled.div`
  width: 95%;
  height: 10%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin: 1rem auto;
  background: rgba(255, 255, 255, 0.25);
  box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
  backdrop-filter: blur(6.0px);
  -webkit-backdrop-filter: blur(6.0px);
  border-radius: 10px;

  div {
    width: 20%;
    margin-right: 2rem;
    display: flex;
    justify-content: space-between;
  }

  .Button {
    width: 100px;
    height: 40px;
    background: rgba(255, 255, 255, 0.25);
    box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
    backdrop-filter: blur(6.0px);
    -webkit-backdrop-filter: blur(6.0px);
    border-radius: 10px;;
    border: none;
    color: #CC72B1;
    font-weight: bold;
    font-size: 1rem;

    :hover {
      cursor: pointer;
      background: rgba(255, 255, 255, 0.65);
    }
    :focus{
      outline: none;
    }
  }
`