import {Switch, Route} from "react-router-dom"
import Main from "../pages/Main";
import InterfaceTwo from "../pages/InterfaceTwo";
import InterfaceOne from "../pages/InterfaceOne";

const Routes = () => {
    return <Switch>
        <Route exact path='/' component={Main}/>
        <Route exact path='/interface_two' component={InterfaceTwo}/>
        <Route exact path='/interface_one' component={InterfaceOne}/>
    </Switch>
}
export default Routes